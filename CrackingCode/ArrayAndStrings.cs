﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CrackingCode
{
    public class ArraysAndStringsAlgorithms
    {

        //Implement an algorithm to determinne if a string has all unique characters. 
        //What if you cannot use additional data structures.
        //1.1
        //Space: O(n) Run: O(n)
        public static bool IsUnique(string word)
        {
            Dictionary<char, int> counter = new Dictionary<char, int>();

            char[] data = word.ToCharArray();

            for(int i=0;  i < data.Length; i++)
            {
                if (counter.ContainsKey(data[i]))
                    return false;
                else
                    counter.Add(data[i], 0);
            }

            return true;
        }

        //Space: O(1) Run: O(n)
        public static bool IsUnique_BitShift(string data)
        {
            char offset = '@';

            Int64 tracker = 0;

            for (int i = 0; i < data.Length; i++)
            {
                //char
                int val = (int)(data[i] - offset);
                
                if( (tracker & (1 << val)) > 0)
                    return false;

                tracker = tracker | (long)(1 << val);
            }
            return true;
        }

        public static bool IsPermutationOfEachOther(string word1, string word2)
        {
            if (word1.Length != word2.Length)                
                return false;

            Dictionary<char, int> tracker = new Dictionary<char, int>();

            foreach (char c in word1.ToCharArray())
            {
                if (tracker.ContainsKey(c))
                    tracker[c]++;
                else
                    tracker.Add(c, 1);
            }

            for(int i=0; i < word2.Length; i++)
            {
                if (tracker.ContainsKey(word2[i]))
                {
                    tracker[word2[i]]--;
                    if (tracker[word2[i]] <= 0) { 
                        tracker.Remove(word2[i]);

                        if (tracker.Count == 0)
                            return true;
                    }
                }

            }

            return false;
        }

        public static char[] URLify(char[] data, int length)
        {
            int writeIdx = data.Length - 1;

            for (int i = length - 1; i > 0; i--)
            {
                if (i == writeIdx)
                    return data;

                if(data[i] == ' ')
                {
                    data[writeIdx - 2] = '%';
                    data[writeIdx - 1] = '2';
                    data[writeIdx - 0] = '0';
                    writeIdx -= 3;
                }
                else
                {
                    data[writeIdx] = data[i];
                    writeIdx--;
                }    

            }

            return data;
        }

        //Assumes words with no punctuation.
        public static bool isPalindrome(string word1)
        {

            Dictionary<char, int> cache = new Dictionary<char, int>();

            //Cache of each letter
            foreach(char c in word1)
            {
                if(c != ' ')
                {
                    if (!cache.ContainsKey(c))
                        cache.Add(c, 1);
                    else
                        cache[c]++;
                }
            }

            int oddCount = 0;
            foreach(var pair in cache)
            {
                if(pair.Value % 2 == 1 || pair.Value == 1)
                {
                    oddCount++;
                    if (oddCount > 1)
                        return false;
                }
            }

            return true;
        }

    }

}
