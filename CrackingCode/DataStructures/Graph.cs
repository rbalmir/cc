﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Diagnostics;
namespace CrackingCode.DataStructures
{
  public class Graph
  {
    Dictionary<int, bool> tracker = new Dictionary<int, bool>();

    public void DFS(Node node)
    {
      if(node != null)
      {
        if (!alreadyVisited(node)) { 
          visit(node);
          node.marked = true;

          foreach (Node childnode in node.Children.OrderBy(d => d.Value))
          {
            if(!childnode.marked)
              DFS(childnode);
          }

        }

      }        
    }

    public void BFS(Node node)
    {
      Queue<Node> q = new Queue<Node>();
      q.Enqueue(node);
      node.marked = true;

      while(q.Count() != 0)
      {
        Node n = q.Dequeue();
        visit(n);

        foreach(Node child in n.Children.OrderBy(d => d.Value))
        {
          if(child.marked == false)
          {
            q.Enqueue(child);
            child.marked = true;
          }
        }

      }

    }

    private void visit(Node node)
    {
      Trace.WriteLine($"Visited:{node.Value}");
    }

    private bool alreadyVisited(Node node) {
      return node.marked;

    }

  }

}
