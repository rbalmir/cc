﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CrackingCode.DataStructures
{
  public class MaxBinaryHeap
  {
    int?[] heap = new int?[(int)Math.Pow(2, 8)];
    int trackend = 1;
    public MaxBinaryHeap()
    { }

    //O(log n)
    //No protection against out of index bounds.
    public void Push(int? val)
    {
      //find last open index
      //int i = findLastOpenIndex();
      int i = this.trackend;

      while(heap[i] != null)
        i++;

      heap[i] = val;
      this.trackend++;

      //exit if root
      if (i == 1)
        return;

      //Make the added value heap compliant
      int childIdx = i;
      siftUp(childIdx);
    }

    public int Pop()
    {
      //Last leaf index
      int i = trackend - 1;

      if (i == 0)
        throw new Exception("No items exist in heap.");

      int maxval = this.heap[1].Value;

      //Replace root with last leaf
      this.heap[1] = this.heap[i];
      this.heap[i] = null;
      this.trackend--;

      int parentIdx = 1;
      
      siftDown(parentIdx);

      return maxval;
    }

    public int[] SortedDestroyed()
    {
      //int last = findLastLeafIndex();
      int last = trackend - 1;

      int[] o = new int[last];

      for(int i=1; i <= last; i++)
      {
        o[i - 1] = Pop();
      }

      return o;
    }

    //Sorted backwards
    //Needs to keep a pointer to last index to support inplace
    public void SortInPlace()
    {
      int last = trackend - 1;

      for (int i = last; i != 0; i--)
      {
        heap[0] = Pop();
        heap[i] = heap[0];
      }

    }

    private void siftDown(int parentIdx)
    {
      bool go = true;
      while (go)
      {
        int childStartIdx = determineChildStartIndex(parentIdx);
        if (childStartIdx > trackend - 1)
          return;

        //Find greater child
        //should check if both children are null then end
        int largerChildIdx = heap[childStartIdx] > heap[childStartIdx + 1] ? childStartIdx : childStartIdx + 1;

        if (heap[parentIdx] < heap[largerChildIdx])
        {
          swap(largerChildIdx, parentIdx);
          parentIdx = largerChildIdx;
        }
        else
        {
          go = false;
        }

      }

    }

    private int findLastLeafIndex()
    {
      int i = findLastOpenIndex();
      i--;

      return i;
    }


    //Can improve by keeping track of additions and removals
    private int findLastOpenIndex()
    {
      int i = 1;
      while (heap[i] != null)
        i++;
      return i;
    }

    private void siftUp(int childIdx)
    {
        //exit if root
        if (childIdx == 1)
            return;

     bool go = true;
      while (go)
      {
        int parentIdx = determineParentIndex(childIdx);

        if (parentIdx == 0)
          return;


                if (parentIdx == 0)
                    return;

        if (parentIdx == 0)
                    return;


        bool isLarger = heap[childIdx].Value > heap[parentIdx].Value;
        if (isLarger)
        {
          //swap child with parent
          //0 index in array can be used instead of variable.
          swap(childIdx, parentIdx);
          
          childIdx = parentIdx;
        }
        else
        {
          go = false;
        }

      }

    }

    private void swap(int childIdx, int parentIdx)
    {
      int? temp = heap[parentIdx];
      heap[parentIdx] = heap[childIdx];
      heap[childIdx] = temp;
    }

    private int determineParentIndex(int index)
    {

      if (index == 1)
        return 0;

      return (int)Math.Floor(index / 2.0);
    }


    private int determineChildStartIndex(int parentIdx)
    {
      return parentIdx * 2;
    }

  }

}
