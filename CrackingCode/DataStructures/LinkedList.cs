﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CrackingCode.DataStructures
{   
  public class LLNode<T>
  {
    public T Item;
    public LLNode<T> NextItem = null; 
  }

  /*
  As long as the use Node<T> is encapsulated.
  The internals of the class will prevent circular references from occuring.
  */
  public class SinglyLinkedList<T>
  {
    private LLNode<T> startNode = null;
       
    public void Add(T item)
    {
      LLNode<T> n = new LLNode<T>();
      n.Item = item;

      if (startNode == null)
        startNode = n;
      else
        lastNode().NextItem = n;
    }

    //Insertion opertion is O(1)
    //Search to the insertion point is O(n)
    public void Insert(T item, int index)
    {
      LLNode<T> newNode = new LLNode<T>();
      newNode.Item = item;


      LLNode<T> tempNode = getItemAt(index);
      newNode.NextItem = tempNode.NextItem;
      tempNode.NextItem = newNode;
    }

    //Search is O(n)
    public T GetItemAt(int index)
    {
      LLNode<T> tempNode = getItemAt(index);
      return tempNode.Item;
    }

    //Removal opertion is O(1)
    //Search to the removal point is O(n)
    public T RemoveItemAt(int index)
    {
      if (index == 0)
        startNode = startNode.NextItem;

      LLNode<T> oneBefore = getItemAt(index - 1);
      LLNode<T> nodeToRemove = oneBefore.NextItem;
      oneBefore.NextItem = oneBefore.NextItem.NextItem;

      return nodeToRemove.Item;
    }


    private LLNode<T> getItemAt(int index)
    {
      LLNode<T> tempNode = startNode;
      for (int i = 0; i <= index; i++)
      {
        if (index == i)
          return tempNode;

        if (tempNode.NextItem != null)
          tempNode = tempNode.NextItem;
        else
          throw new IndexOutOfRangeException("SinglyLinkedList");
      }

      return null;
    }


    private LLNode<T> lastNode()
    {
      if (startNode == null)
        return null;

      LLNode<T> tempNode = startNode;
      while(tempNode.NextItem != null)
        tempNode = tempNode.NextItem;

      return tempNode;
    }

  }
   
}
