﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CrackingCode.DataStructures
{
    public class Node
    {
      public int Value;
      public List<Node> Children = new List<Node>();
      public bool marked = false;
    }
}
