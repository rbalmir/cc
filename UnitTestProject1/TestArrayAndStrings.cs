using Microsoft.VisualStudio.TestTools.UnitTesting;
using CrackingCode;
namespace UnitTestProject1
{
    [TestClass]
    public class TestArrayAndStrings
    {
        [TestMethod]
        public void IsUnique()
        {
            var word1 = "abcdefg";
            bool expectedTrue = ArraysAndStringsAlgorithms.IsUnique(word1);

            Assert.IsTrue(expectedTrue);
        }

        [TestMethod]
        public void IsUniqueByBitShift()
        {
            var word1 = "abcdefg";
            bool expectedTrue = ArraysAndStringsAlgorithms.IsUnique_BitShift(word1);

            Assert.IsTrue(expectedTrue);
        }


        [TestMethod]
        public void IsUnique02()
        {
            var word1 = "abbcdefg";
            bool expectedFalse = ArraysAndStringsAlgorithms.IsUnique(word1);
            Assert.IsFalse(expectedFalse);
        }

        [TestMethod]
        public void IsPermuation01()
        {
            var word1 = "abbc";
            var word2 = "ffncadsweafdsacdsfewjbbwedfsaeakwei234801922e";
            bool ans = ArraysAndStringsAlgorithms
                .IsPermutationOfEachOther(word1,word2);

            Assert.IsTrue(ans);
        }

        [TestMethod]
        public void IsPermuation02()
        {
            var word1 = "abbcz";
            var word2 = "ffncads1922e";
            bool ans = ArraysAndStringsAlgorithms
                .IsPermutationOfEachOther(word1, word2);

            Assert.IsFalse(ans);
        }

        [TestMethod]
        public void URLify()
        {
            var testString = "ab cd  ";
            var expectedOutput = "ab%20cd";

            char[] encoded = ArraysAndStringsAlgorithms
                                .URLify(testString.ToCharArray(), testString.Trim().Length);

            Assert.AreEqual(expectedOutput, new string(encoded));
        }

        [TestMethod]
        public void PermutationOfPalindrome()
        {
            var testString = "taco cat";

            bool ans =  ArraysAndStringsAlgorithms
                                .isPalindrome(testString);

            Assert.IsTrue(ans);
        }


    }
}
