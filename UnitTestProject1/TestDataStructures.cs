using Microsoft.VisualStudio.TestTools.UnitTesting;
using CrackingCode;
using CrackingCode.DataStructures;
namespace UnitTestProject1
{


  [TestClass]
  public class TestDataStructures
  {
    [TestMethod]
    public void TestHeap()
    {
      int?[] arr = new int?[] { 5, 7, 9, 10, 1, 2 , 12, 20, 4, 5};

      var heap = new MaxBinaryHeap();

      foreach (int? a in arr)
      {
        heap.Push(a);
      }

      //heap.Pop();
      heap.SortInPlace();

      Assert.IsTrue(true);
    }

  }

}
